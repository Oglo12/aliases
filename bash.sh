#!/usr/bin/env bash

command -v nvim > /dev/null 2>&1 && export MANPAGER="nvim +Man!"

# Configuration sourcing... (This alias configuration repository is for me, not others, so this works fine...)
[[ -d ~/.config/helix/.git ]] && source ~/.config/helix/lib/bash.sh
[[ -d ~/.config/nvim/.git ]] && source ~/.config/nvim/lib/bash.sh
[[ -d ~/.config/starship/.git ]] && source ~/.config/starship/lib/bash.sh
[[ -d ~/.config/zellij/.git ]] && source ~/.config/zellij/lib/bash.sh

# I can't remember exactly why this is here, but I
# think it is so I have truecolor support when SSHing.
# I could be wrong.
if [[ "$TERM" != "linux" ]]; then
	export TERM=xterm
	export COLORTERM=truecolor
fi

#############################

terminal_games=(
    "tui48;"
    "bastet;"
    "nudoku;"
    "ninvaders;"
    "nsnake;"
    "myman --size=big;"
    "moon-buggy;"
    "greed;"
)

krabby_pool=(
	"solosis"
	"duosion"
	"budew"
	"wormadam"
	"grookey"
	"steenee"
	"bounsweet"
	"tsareena"
	"fomantis"
	"charjabug"
	"decidueye"
	"zygarde"
	"dartrix"
	"hawlucha"
	"florges"
	"chesnaught"
	"quilladin"
	"chespin"
	"virizion"
	"haxorus"
	"fraxure"
	"axew"
	"ferrothorn"
	"ferroseed"
	"reuniclus"
	"trubbish"
	"garbodor"
	"maractus"
	"petilil"
	"lilligant"
	"sewaddle"
	"swadloon"
	"leavanny"
)

# I like fries...
alias play-fried='play --volume 99999999'

# Fix weirdness when exiting on a TTY...
alias exit='reset; exit'

# I use << INSERT REALLY GOOD DISTRO HERE >>, btw.
alias oh-btw='clear && fastfetch'

# TRON
alias tron-online='ssh sshtron.zachlatta.com'

# Hyprland Aliases
alias hyprctl-current='hyprctl --instance 0'

# Random Satisfying Aliases
alias pipes-rs-drool='pipes-rs -c rgb --rainbow 1 -p 5'

# Extra Binds
alias cls='clear && print-rc'
alias celar='clear'

# Kill Steam
alias kill-steam='while [[ 1 == 1 ]]; do killall steam; pkill steam; done'

# Fetch Alias
alias fetch='fastfetch'

# Cargo
alias cr='cargo run --'
alias ccr='clear && cr'
alias cargo-install='cargo install --path .'
alias cargo-upgrade='cargo install-update -a'

# Python
alias py='python'

# Vim-like
alias :q='exit'
alias :wq='exit'

# TTY Clock Settings
alias clock='tty-clock -ctC 6'

# Lavat
alias lavat-ready='lavat -c cyan -R 4 -b 20 -r 2'

# Rebinding
command -v eza > /dev/null 2>&1 && alias ls='eza -a --icons'
command -v bat > /dev/null 2>&1 && alias cat='bat'

# Line count alias.
alias lc='wc -l'

# Cursor Stuff
alias back-to-line='echo -e "\r\033[A\033[A"'
alias move-up='back-to-line && back-to-line'

# Pipes Program
alias empty='pipes.sh -t c"                "'
alias block-pipes='pipes.sh -t c"████████████████"'

# Firewall
alias kfd='sudo killall firewalld'

# Just Some Fun Stuff
alias error.exe='while [[ 1 == 1 ]]; do notify-send "Uh oh!" "There is a fatal error!" -u critical; done'

# Battery Level
alias battery-level="echo \"$(acpi -b 2> /dev/null | grep -P -o '[0-9]+(?=%)' | sed 's/ 0//g' | head --lines=1)%\""
alias bl='battery-level'

# FZF Scripts
alias yay-fzf='yay -Slq | fzf --multi --preview "yay -Si {1}" | xargs -ro yay -S'

# Get public IP address.
public-ip () {
    echo "$(curl https://ipecho.net/plain 2> /dev/null | tail --lines 1)"
}

# List IP link devices.
ls-ip-links () {
    ip link show | grep -E '^[0-9]:' | sed 's/: /:/g' | cut -f2 -d ':' | grep -vE '^lo$'
}

# Bypass Mac Address Internet Blocking (Pre-Filled)
bmab-auto () {
    [[ -d ~/.cache ]] || mkdir -p ~/.cache

    use_menu=0
    if [[ "$1" == "--choose" ]] || [[ "$1" == "choose" ]] || [[ "$1" == "-c" ]] || [[ "$1" == "c" ]]; then
        use_menu=1
    fi

    auto_path="$HOME/.cache/bmab_device"

    bmab_device=""

    if [[ -f "$auto_path" ]] && [[ "$use_menu" == 0 ]]; then
        bmab_device="$(cat "$auto_path")"
    else
        if command -v fzf > /dev/null 2>&1; then
            bmab_device="$(ls-ip-links | fzf '--height=20%' --border=rounded)"

            echo "$bmab_device" > "$auto_path"
        else
            echo "FZF is not installed!" >&2

            return 1
        fi
    fi

    echo -e "Target: '\033[1;96m${bmab_device}\033[0m'"

    bmab-rand "${bmab_device}" || return 1
}

# Uptime Timer
alias uptick='clear; while [[ 1 == 1 ]]; do echo -en "\r$(uptime -p)\033[A"; done'

# Check To See If System Is BIOS Or UEFI
alias bios-or-uefi='[[ -d /sys/firmware/efi ]] && echo "UEFI" || echo "BIOS"'

# Quick CDs
alias qcd-pmi='cd ~/.var/app/org.polymc.PolyMC/data/PolyMC/instances'

# Why such a long command for something so simple!?
alias get-framerate='ffprobe -v error -select_streams v:0 -show_entries stream=avg_frame_rate -of default=noprint_wrappers=1:nokey=1'

# Flameshot Wayland capture.
flameshot-wayland-capture () {
    XDG_CURRENT_DESKTOP=sway flameshot gui --raw | wl-copy
}

# Open Git repository remote URL in browser.
git-remote-url-open () {
    if [[ "$1" == "" ]]; then
        echo "Please specify the remote to use! (Commonly: \"origin\")" >&2
        echo "You can also optionally specify a browser to use as a second argument." >&2
        return 1
    fi

    if [[ -d "./.git" ]]; then
        if git remote get-url "$1" > /dev/null 2>&1; then
            echo "Opening URL: '$(git remote get-url "$1" | sed 's/.git$//')'"
        else
            echo "Not a valid remote!" >&2
            return 1
        fi
    else
        echo "Not a valid Git repository!" >&2
        return 1
    fi

    open_with="xdg-open"

    [[ "$2" == "" ]] || open_with="$2"

    $open_with "$(git remote get-url "$1" | sed 's/.git$//')" > /dev/null 2>&1 & disown
}

# Is SysRQ Enabled
is-sysrq-enabled () {
    if [[ "$(cat /proc/sys/kernel/sysrq)" == "1" ]]; then
        echo "SysRQ is enabled! (^-^)"
    else
        echo "SysRQ is not enabled! (T~T)"
        return 1
    fi
}

# Roll Dice
dice-roll () {
    num_sides=6

    [[ "$1" == "" ]] || num_sides="$1"

    echo $(( $(( ${RANDOM} % ${num_sides} )) + 1 ))
}

# Just run a bunch of botted 2048 games in a loop.
tui48-botted-loop () {
    while true; do
        tui48 --bot ~/Coding/Rust/tui48/example-tui48-bot.sh --bot-file /tmp/2048-bot-file --bot-delay 0.2 $@
        sleep 0.2
    done
}

# Play A Quick Game
play-terminal-game () {
    $(echo ${terminal_games[@]} | tr ';' '\n' | awk '!/^$/ { print }' | xargs -I {} bash -c 'command -v {} > /dev/null 2>&1 && echo {}' | fzf '--height=50%' --border=rounded)
}

play-terminal-game-random () {
    command_full="$(echo "${terminal_games[$(($RANDOM % ${#terminal_games[@]}))]}" | sed 's/;$//')"
    command_bin="$(echo "$command_full" | cut -f1 -d ' ')"

    if command -v "${command_bin}" > /dev/null 2>&1; then
        ${command_full}
    else
        echo "Command '${command_bin}' doesn't exist... choosing again..."
        play-terminal-game-random
    fi
}

ls-terminal-games () {
		THE_COMMAND="echo -n \"\$(echo '{}' | cut -f1 -d ' ') ... \"; command -v {} > /dev/null 2>&1 && echo -en '\033[1;32mFOUND' || echo -en '\033[1;31mNOT FOUND'; echo -e '\033[0m'"

		echo ${terminal_games[@]} | tr ';' '\n' | awk '!/^$/ { print }' | xargs -I {} bash -c "${THE_COMMAND}"
}

# Quick Navigate
coding () {
	cd "$HOME/Coding/$1"
	ls
}

# Switch Shell Now Command
ssn () {
	clear
	$1
	exit
}

# Switch Shell Only If It Is The Parent Shell (Startup Shell)
ssn-if-not-child-of () {
	parent_pid=$(ps -o ppid= -p $$ | sed 's/^  //g')
	parent_command=$(ps -o comm= -p $parent_pid)

	[[ "$parent_command" == "$1" ]] || ssn "$1"
}

# Random Two Digit Hex Number (Byte)
rand-byte-hex () {
    openssl rand -hex 1 2> /dev/null || head -c1 /dev/urandom | xxd -p
}

# Random Machine Address (MAC Address)
rand-mac-address () {
    echo "$(rand-byte-hex):$(rand-byte-hex):$(rand-byte-hex):$(rand-byte-hex):$(rand-byte-hex):$(rand-byte-hex)"
}

# Show MAC Address
mac-addr-show () {
    ip addr show | grep -A 1 "${BMAB_DEVICE}" | head --lines 2 | tail --lines 1 | sed 's/^ \+//' | cut -f2 -d ' '
}

# Bypass MAC Address Internet Blocking
bmab () {
	sudo ip link set dev "$1" down || return 1
	sudo ip link set dev "$1" address "$2" || return 1
	sudo ip link set dev "$1" up || return 1
}

bmab-rand () {
    if [[ "$1" == "" ]]; then
        echo "No device specified!" >&2

        return 1
    fi

    prefix="MAC Address: "

    while true; do
        cursor_move_up=0

        mac_address="$(rand-mac-address)"

        if bmab "$1" "$mac_address" > /dev/null 2>&1; then
            echo -e "${prefix}\033[0;92m${mac_address}\033[0m"

            return 0
        else
            echo -e "${prefix}\033[0;91m${mac_address}\033[0m"
        fi
        cursor_move_up=$(( $cursor_move_up + 1 ))

        echo -en "\033[${cursor_move_up}A"
        sleep 0.1
        tput ed
    done
}

# Create SSH Tunnel For VNC Server/Client
vnc-ssh-tunnel () {
	ssh -L 5901:127.0.0.1:5901 -C -N -l "$1" "$2"
}

# Run command more than once.
mrun () {
    local times="$1"
    shift 1

	for i in $(seq 1 "$times"); do
        "$@"
	done
}

# Clean system.
clean-system () {
	rm -rf ~/.cache
}

# Restart a program.
refresh () {
	killall $1
	$1 &
}

# Monitor stuff.
ls-monitors () {
	xrandr | grape " connected" | cut -f1 -d " "
}

set-primary-monitor () {
	success="no"

	for i in $(ls-monitors); do
		if [[ $i == $1 ]]; then
			xrandr --output $i --primary
			success="yes"
		fi
	done

	if [[ $success == "no" ]]; then
		echo "Invalid monitor!"
	fi
}

# Fix QEMU 'default' network not working!
enable-qemu-net () {
	sudo virsh net-autostart default
	sudo virsh net-start default
}

# Add Flathub to Flatpak!
add-flathub () {
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

# Fix issues with Git.
fix-git () {
	git config --global http.postBuffer 157286400
	git config --global https.postBuffer 157286400
}

# Attempt to fix any issues.
fix-system () {
	enable-qemu-net
	add-flathub
	fix-git
}

# Select from the Krabby pool.
krabby-pool () {
	entity_entry=${krabby_pool[$(($RANDOM % ${#krabby_pool[@]}))]}

	entity_id=$(echo "$entity_entry" | cut -f1 -d ":")
	entity_name=$(echo "$entity_entry" | cut -f2 -d ":")

	line_count=$(krabby name "$entity_id" | wc -l)

	krabby name "$entity_id" | tail --lines=$(($line_count - 2))
}

# Make an executable file.
mkexec () {
	if [[ "$1" == "" ]]; then
		echo "Please specify a file path!"
	elif [[ -f "$1" ]]; then
		echo "File already exists!"
	else
		touch "$1"
		chmod +x "$1"
	fi
}

# VNC Stuff
start-vnc () {
	vnc_res=1920x1080

	[[ "$1" == "" ]] || vnc_res="$1"

	echo -e "\nStarting with resolution: $vnc_res"

	vncserver -geometry "$vnc_res"
}

stop-vnc () {
	vnc_display=1

	[[ "$1" == "" ]] || vnc_display="$1"

	vncserver -kill ":$vnc_display" && echo "Killed display: $vnc_display" || echo -e "Failed to kill display: ${vnc_display}\n"
}

# Upgrade everything on the system. (Package Managers)
upgrade-pkgs () {
	if [[ -f /etc/pacman.conf ]]; then
		if command -v paru > /dev/null 2>&1; then
			paru -Syyu
		else
			sudo pacman -Syyu
		fi
	fi

	if command -v apt > /dev/null 2>&1; then
		sudo apt update -y && sudo apt upgrade
	fi

	if command -v dnf > /dev/null 2>&1; then
		sudo dnf upgrade
	fi

	if command -v cargo > /dev/null 2>&1; then
		if cargo install --list | grep "cargo-update" > /dev/null 2>&1; then
			cargo install-update -a
		fi
	fi

	if command -v flatpak > /dev/null 2>&1; then
		flatpak upgrade
	fi
}

# Fix Getuto if it freezes. (Gentoo Linux)
getuto-fix () {
    sudo rm -rf /etc/portage/gnupg || return 1
    echo 'Now try running "getuto" again!'
}

# Install an SDDM theme.
sddm-theme-install () {
    if [[ "$1" == "" ]]; then
        echo "Please specify the theme directory path to install!" >&2
        return 1
    fi

    if [[ "$2" == "" ]]; then
        echo "Please specify the theme name to save as!" >&2
        return 1
    fi

    if [[ -d "$1" ]]; then
        echo "Installing '$1' as theme: '$2'"
    else
        echo "No such directory!" >&2
        return 1
    fi

    if [[ -f "$1/Main.qml" ]]; then
        echo "Verified that the theme is a valid SDDM theme!"
    else
        echo "Not a valid SDDM theme! (Missing: Main.qml)" >&2
        return 1
    fi

    if [[ -d "/usr/share/sddm/themes/$2" ]]; then
        echo "Theme already installed! Skipping..."
        return 0
    fi

    [[ -d /usr/share/sddm/themes ]] || sudo mkdir -p /usr/share/sddm/themes
    sudo cp -r "$1" "/usr/share/sddm/themes/$2" || return 1

    echo "Theme '$2' has been installed!"
}

# Uninstall an SDDM theme.
sddm-theme-remove () {
    if [[ "$1" == "" ]]; then
        echo "Please specify theme name!" >&2
        return 1
    fi

    if [[ -d "/usr/share/sddm/themes/$1" ]]; then
        echo "Uninstalling theme: '$1'"
        sudo rm -rf "/usr/share/sddm/themes/$1" || return 1
        echo "Theme '$1' has been uninstalled!"
    else
        echo "Theme '$1' doesn't exist!" >&2
        return 1
    fi
}

# Preview the current SDDM theme.
sddm-theme-preview () {
    if [[ -f /etc/sddm.conf ]]; then
        echo "Found SDDM config file!"
    else
        echo "SDDM config file doesn't exist! ('/etc/sddm.conf')" >&2
        return 1
    fi

    current_theme="$(cat /etc/sddm.conf | grep 'Theme' -A 1 | grep 'Current' | cut -f2 -d '=')"

    if [[ "$current_theme" == "" ]]; then
        echo "Failed to get theme information from the SDDM config!" >&2
        return 1
    fi

    sddm-greeter --test-mode --theme "/usr/share/sddm/themes/${current_theme}"
}

# View the differences between the latest Git commit offset back by $1, and the latest commit offset back by $2.
git-index-diff () {
    cmd_usage="Usage: git-index-diff <OLD> <NEW>"

    if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
        echo "$cmd_usage" >&2
        return 1
    fi

    [[ -d ./.git ]] || echo "Not in a Git repository directory!" >&2
    [[ -d ./.git ]] || return 1

    new_commit="$(git log | grep "^commit " | head --lines $((1 + $2)) | tail --lines 1 | cut -f2 -d ' ')"
    old_commit="$(git log | grep "^commit " | head --lines $((1 + $1)) | tail --lines 1 | cut -f2 -d ' ')"

    git diff $old_commit $new_commit
}

# View differences between the latest Git commit and the commit before it.
git-latest-diff () {
    git-index-diff 1 0 || return 1
}

# Print out all the color IDs.
all-the-colors () {
    for i in $(seq 30 36); do
        echo -e "\033[1;${i}m${i}:\033[0m \033[0;${i}m(^-^)\033[0m"
    done
}

# Create raw binary files of a certain size.
touch-bin () {
    if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
        echo "Usage: touch-bin <FILE> <SIZE_IN_BYTES>"

        return 1
    fi

    dd if=/dev/zero "of=$1" bs=1 "count=$2"
}

# Git tag a project with the Rust crate version
git-tag-crate-version () {
    if [[ -f ./Cargo.toml ]]; then
        if grep 'version' ./Cargo.toml > /dev/null 2>&1; then
            crate_version="$(grep 'version' ./Cargo.toml | sed 's/ //g' | cut -f2 -d '=' | sed 's/"//g' | head --lines 1)"
            final_tag="v${crate_version}"

            echo "Final Tag: ${final_tag}"

            while true; do
                read -p "Continue? [y/n]: " user_answer

                if [[ "$user_answer" == "y" ]]; then
                    git tag "${final_tag}" || return 1
                    return 0
                else
                    echo "Aborting..." >&2
                    return 1
                fi

                echo "Invalid answer, please answer either 'y' or 'n'..."
            done
        else
            echo "Failed to get crate version!" >&2
            return 1
        fi
    else
        echo "Not in a valid Rust project! (Cargo.toml not found!)" >&2
        return 1
    fi
}

# YT-DLP but it is easy...
yt-dlp-easy () {
    if [[ "$1" == "" ]] \
    || [[ "$1" == "help" ]] \
    || [[ "$1" == "-h" ]] \
    || [[ "$1" == "--help" ]] \
    || [[ "$2" == "" ]] \
    || [[ "$3" == "" ]]; then
        echo "Usage: yt-dlp-easy <VIDEO URL> <VIDEO HEIGHT> <VIDEO FPS>"
        echo "To get the necessary information, use: 'yt-dlp -F <VIDEO URL>'"
    else
        v_url="$1"
        v_height="$2"
        v_fps="$3"

        echo "Video Height: ${v_height}"
        echo "Video FPS: ${v_fps}"

        echo -e "\nExecuting yt-dlp...\n"

        time yt-dlp -f "bestvideo[height=${v_height}][fps=${v_fps}]+bestaudio/best" \
            -o '%(title)s.%(ext)s' \
            --merge-output-format mp4 "${v_url}" || return 1

        echo -e "\nDone!"
    fi
}

# CD into a mountpoint.
cd-mount () {
    cd_point="$(lsblk -o mountpoints | grep -v '^$' | grep '^/' | fzf --height ~50% --border rounded)"

    [[ "$cd_point" == "" ]] && return 1

    cd "$cd_point" || return 1
}

# Update aliases.
update-aliases () {
    [[ "$ALIAS_DIR" == "" ]] && echo '$ALIAS_DIR unset, not updating...' >&2

    start_dir="$(pwd)"

    if [[ "$ALIAS_DIR" != "" ]]; then
        if cd "$ALIAS_DIR"; then
            git pull origin main > /dev/null 2>&1
        else
            echo "Failed to change directory into: '${ALIAS_DIR}'" >&2
        fi
    fi

    cd "$start_dir" || echo "Failed to change directory into: '${start_dir}'" >&2
}

background-update () {
    update-aliases > /dev/null & disown
}

# PrintRC
print-rc () {
	if command -v cermic > /dev/null 2>&1; then
		eval "cermic 1 $HOME/.cermic_repo && echo ' '" || echo "PRINTRC FAILED" > /dev/null
	fi
}



#### Autostart ####
if [[ "$TERM" == "linux" ]]; then
    clear
fi

print-rc

background-update
###################
