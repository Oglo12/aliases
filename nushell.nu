# Print-RC
def print-rc [] {
  cermic 1 ~/.cermic_repo
  echo " "
}

# Aliases
alias cat = bat
alias c = clear

# Functions
def clock [] { tty-clock -ctbC 4 }

# Sloppy Fix
alias celar = clear

# Startup.
print-rc
